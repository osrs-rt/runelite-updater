﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;

namespace Runelite_Updater
{
    public class MainWindowViewModel : BaseViewModel
    {
        private int _currentProgress;
        private ICommand _download;
        private BackgroundWorker _worker = new BackgroundWorker();
        private string _downloadPercentage;
        private MainWindowModel _model;
        private string _downloadInfo = "Downloading";
        private string _totalPercentage;
        private Exception _downloadError;

        public MainWindowViewModel()
        {
            _model = new MainWindowModel();
            _model.PercentageChanged += OnPercentageChanged;

            _worker.DoWork += DoWork;
            _worker.ProgressChanged += ProgressChanged;
            _worker.WorkerReportsProgress = true;
            _worker.WorkerSupportsCancellation = true;

            CurrentProgress = 0;
        }

        private void OnPercentageChanged(object sender, EventArgs<int> e)
        {
            DownloadPercentage = e.Value.ToString() + "%";
            CurrentProgress = e.Value;
        }

        public int CurrentProgress
        {
            get { return _currentProgress; }
            set
            {
                if (_currentProgress != value)
                {
                    _currentProgress = value;
                    OnPropertyChanged("CurrentProgress");
                }
            }
        }

        public string DownloadPercentage
        {
            get { return _downloadPercentage; }
            set
            {
                if (_downloadPercentage != value)
                {
                    _downloadPercentage = value;
                    OnPropertyChanged("DownloadPercentage");
                }
            }
        }

        public string TotalPercentage
        {
            get { return _totalPercentage; }
            set
            {
                if (_totalPercentage != value)
                {
                    _totalPercentage = value;
                    OnPropertyChanged("TotalPercentage");
                }
            }
        }

        public string DownloadInfo
        {
            get { return _downloadInfo; }
            set
            {
                if (_downloadInfo != value)
                {
                    _downloadInfo = value;
                    OnPropertyChanged("DownloadInfo");
                }
            }
        }

        public Exception DownloadException {
            get { return _downloadError; }
            set
            {
                if (_downloadError != value)
                {
                    _downloadError = value;
                    OnPropertyChanged("DownloadError");
                }
            }
        }


        public ICommand Download
        {
            get
            {
                return _download ?? (_download = new RelayCommand(x =>
                {
                    DownloadContent();
                }));
            }
        }

        public void DownloadContent()
        {
            _worker.RunWorkerAsync();
        }

        private void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            CurrentProgress = e.ProgressPercentage;
        }

        private async void DoWork(object sender, DoWorkEventArgs e)
        {
            if (CurrentProgress >= 100)
            {
                CurrentProgress = 0;
            }


            DownloadInfo = "Downloading";
            // TODO: CHANGE INFO BASED ON STATUS CODE OF BELOW
            try
            {
                await _model.DownloadRuneliteClient("https://osrs-rt-graphql.azurewebsites.net/download", new CancellationTokenSource().Token);
                _model.RunJar();
                _model.Exit();
            } catch (Exception ex)
            {
                DownloadInfo = "Failed";
                DownloadPercentage = ex.Message;
            }

        }
    }
}