﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Runelite_Updater
{
    public class MainWindowModel
    {
        private static readonly string _folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "runelite-rt");
        private static readonly string _settingsPath = Path.Combine(_folder, "runelite.settings");
        private static readonly string _jarPath = Path.Combine(_folder, "runelite.jar");
        //private static readonly string _javaPath = Path.Combine(Environment.GetEnvironmentVariable("JAVA_HOME"), "bin", "javaw.exe");
        private static readonly string _javaExecutable = "javaw.exe";

        private Dictionary<string, string> _settings = new Dictionary<string, string>();

        public MainWindowModel()
        {
            if (!File.Exists(_settingsPath)) return;
            var lines = File.ReadAllLines(_settingsPath);
            foreach (var line in lines)
            {
                var kvp = line.Split("=");
                if (kvp.Length != 2) continue;
                _settings.Add(kvp[0].Trim(new char[] { '"', '\t', ' ' }), kvp[1].Trim(new char[] { '"', '\t', ' ' }));
            }
        }

        private void SaveSettings()
        {
            Directory.CreateDirectory(_folder);
            var settings = _settings.Select(kvp => $"{kvp.Key}={kvp.Value}");
            File.WriteAllLines(_settingsPath, settings);
        }

        public event EventHandler<EventArgs<int>> PercentageChanged;
        public async Task DownloadRuneliteClient(string path, CancellationToken token)
        {
            Directory.CreateDirectory(_folder);

            using (HttpClient client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Get, path))
            {
                if (_settings.ContainsKey("currentVersion"))
                {
                    request.Headers.IfNoneMatch.Add(new EntityTagHeaderValue(
                        $"\"{_settings["currentVersion"]}\""));
                }

                using (HttpResponseMessage response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, token))
                {
                    if (response.StatusCode == HttpStatusCode.NotModified)
                    {
                        PercentageChanged.Raise(this, 100);
                        return;
                    }

                    response.EnsureSuccessStatusCode();

                    if (response.Headers.ETag != null)
                    {
                        _settings["currentVersion"] = response.Headers.ETag.Tag;
                        SaveSettings();
                    }


                    var total = response.Content.Headers.ContentLength.HasValue ?
                       response.Content.Headers.ContentLength.Value : -1L;

                    var canReportProgress = total != -1;

                    using (var stream = await response.Content.ReadAsStreamAsync())
                    using (var fileStream = File.Create(_jarPath))
                    {
                        var totalRead = 0L;
                        var buffer = new byte[4096];
                        var moreToRead = true;

                        do
                        {
                            token.ThrowIfCancellationRequested();

                            var read = await stream.ReadAsync(buffer, 0, buffer.Length, token);

                            if (read == 0)
                            {
                                moreToRead = false;
                            }
                            else
                            {
                                var data = new byte[read];
                                buffer.ToList().CopyTo(0, data, 0, read);
                                fileStream.Write(data);

                                // TODO: write the actual file to disk

                                // Update the percentage of file downloaded
                                totalRead += read;

                                if (canReportProgress)
                                {
                                    var downloadPercentage = ((totalRead * 1d) / (total * 1d)) * 100;
                                    var value = Convert.ToInt32(downloadPercentage);

                                    PercentageChanged.Raise(this, value);
                                }
                            }
                        }
                        while (moreToRead);
                    }
                }
            }
        }

        public void RunJar()
        {
            try
            {
                StartJarUsingPath();
                return;
            }
            catch (Win32Exception)
            {
                // Swallow file not found exception so we try next path. Other excpetions will bubble.
            }

            try
            {
                StartJarUsingJavaHome();
                return;
            }
            catch (Win32Exception)
            {
                // Swallow file not found exception so we try next path. Other excpetions will bubble.
            }

            StartJarUsingJdkHome();
        }

        private void StartJarUsingPath()
        {
            Start(_javaExecutable);
        }

        private void StartJarUsingJavaHome()
        {
            var path = Path.Combine(Environment.GetEnvironmentVariable("JAVA_HOME"), "bin", _javaExecutable);
            Start(path);
        }

        private void StartJarUsingJdkHome()
        {
            var path = Path.Combine(Environment.GetEnvironmentVariable("JDK_HOME"), "bin", _javaExecutable);
            Start(path);
        }

        private void Start(string path)
        {
            List<string> allArgs = new List<string>();
            allArgs.Add("-jar");
            allArgs.Add(_jarPath);

            allArgs.AddRange(App.CommandLineArgs);

            string args = string.Join(" ", allArgs);
            ProcessStartInfo startInfo = new ProcessStartInfo(path, args);
            Process.Start(startInfo);
        }

        public void Exit()
        {
            Environment.Exit(0);
        }
    }
}