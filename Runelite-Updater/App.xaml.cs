﻿using System.Collections.Generic;
using System.Windows;

namespace Runelite_Updater
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string[] CommandLineArgs;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            CommandLineArgs = e.Args;
            new MainWindow().Show();
        }
    }
}
